package com.gwylim.common;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.util.concurrent.atomic.AtomicBoolean;

public class DaemonLoopTests {
    @Test
    void terminate() throws InterruptedException {

        final AtomicBoolean running = new AtomicBoolean(false);
        final AtomicBoolean cleanedUp = new AtomicBoolean(false);

        final ThreadUtil.DaemonController controller = ThreadUtil.daemonLoop(
                () -> running.set(true),
                () -> cleanedUp.set(true)
        );

        // Check that the daemon is running:
        Assertions.assertTrue(
                ThreadUtil.waitFor(running::get, Duration.ofSeconds(1)),
                "Daemon has not started running."
        );

        controller.stop();

        // Stop completing means that cleanup should have occurred:
        Assertions.assertTrue(
                cleanedUp.get(),
                "Daemon cleanup has not been run."
        );

        running.set(false);

        // Check that Daemon is no-longer running:
        Assertions.assertFalse(
                ThreadUtil.waitFor(running::get, Duration.ofSeconds(1)),
                "Daemon has not terminated."
        );
    }
}
