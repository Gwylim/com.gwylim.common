package com.gwylim.common;

import com.gwylim.common.Console.SupersededException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.time.Duration;
import java.util.concurrent.atomic.AtomicBoolean;

import static java.nio.charset.StandardCharsets.UTF_8;

@Disabled
class ConsoleTests {

    /**
     * Inter-thread interaction tests need a delay...
     * This appears to be the minimum for consistent results.
     */
    private static final Duration DELAY = Duration.ofMillis(1000);

    @Mock
    private PrintStream output;

    private PrintWriter inputTest;

    private Console console;

    @BeforeEach
    void setup() throws IOException {
        MockitoAnnotations.initMocks(this);

        final PipedOutputStream pipedOutputStream = new PipedOutputStream();
        inputTest = new PrintWriter(new OutputStreamWriter(pipedOutputStream, UTF_8));

        console = new Console(new PipedInputStream(pipedOutputStream), output);
    }


    @Test
    void inputTermination() {
        Assertions.assertTimeoutPreemptively(Duration.ofSeconds(5), () -> {
            final Console console = new Console();
            console.terminate();
        });
    }

    /**
     * Test input reading.
     */
    @Test
    void inputReading() {

        Assertions.assertTimeoutPreemptively(Duration.ofSeconds(5), () -> {

            ThreadUtil.run(DELAY, () -> {
                inputTest.println("Test");
                inputTest.flush();
            });
            Thread.yield();
            Assertions.assertEquals("Test", console.read(""));


            final String testValue = "Test2";

            ThreadUtil.run(DELAY, () -> {
                inputTest.println(testValue);
                inputTest.flush();
            });
            Thread.yield();
            Assertions.assertEquals(testValue, console.read(""));

        });
    }

    /**
     * Test input reading.
     * Data in the buffer before the prompt is given is not read.
     */
    @Test
    void inputReadingOrder() {

        Assertions.assertTimeoutPreemptively(Duration.ofSeconds(5), () -> {

            inputTest.println("Test");
            inputTest.flush();

            ThreadUtil.run(DELAY, () -> {
                inputTest.println("Test2");
                inputTest.flush();
            });
            Thread.yield();
            Assertions.assertEquals("Test2", console.read(""));

        });
    }

    /**
     * Test that one thread can supersede another and will receive input.
     */
    @Test
    void superseding() {

        Assertions.assertTimeoutPreemptively(Duration.ofSeconds(5), () -> {

            final AtomicBoolean superseded = new AtomicBoolean(false);

            ThreadUtil.run(() -> {
                try {
                    final String line = console.read("Other Thread Read.");
                    Assertions.fail("Did not expect to receive line: " + line);
                }
                catch (final SupersededException e) {
                    System.out.println(e.getClass().getSimpleName());
                    superseded.set(true);
                }
            });

            ThreadUtil.run(DELAY, () -> {
                inputTest.println("Test");
                inputTest.flush();
            });
            Thread.yield();
            Assertions.assertEquals("Test", console.read(""));

            Assertions.assertTrue(superseded.get());

        });
    }

    /**
     * Test input reading and converting to a specific type.
     */
    @Test
    void inputConversion() {

        Assertions.assertTimeoutPreemptively(Duration.ofSeconds(5), () -> {

            final int testValue = 1123123;

            ThreadUtil.run(DELAY, () -> {
                inputTest.println("Test");
                inputTest.flush();

                ThreadUtil.run(DELAY, () -> {
                    inputTest.println(testValue);
                    inputTest.flush();
                });
            });
            Thread.yield();

            Assertions.assertEquals(Integer.valueOf(testValue), console.read(Integer.class,  ""));
        });
    }
}
