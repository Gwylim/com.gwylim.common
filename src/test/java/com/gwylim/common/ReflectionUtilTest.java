package com.gwylim.common;

import org.junit.jupiter.api.Test;

import java.util.function.Consumer;

public class ReflectionUtilTest {
    @Test
    public void test3() {

        final Class<?> typeArg = ReflectionUtil.getGenericClass(((Consumer<Integer>) (y) -> { }).getClass(), Consumer.class);

        assert typeArg == Integer.class;
    }
}
