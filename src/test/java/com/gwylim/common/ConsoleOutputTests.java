package com.gwylim.common;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.io.PrintStream;

class ConsoleOutputTests {

    @Mock
    private PrintStream out;

    private ConsoleOutput console;

    @BeforeEach
    void setup() {
        MockitoAnnotations.initMocks(this);

        console = new ConsoleOutput(out);
    }

    @Test
    void printf() {

        console.printf("Hello");

        Mockito.verify(out, Mockito.atLeastOnce()).println("Hello");
    }

    @Test
    void printfSingleArg() {
        console.printf("Hello {}.", "Bob");

        Mockito.verify(out, Mockito.atLeastOnce()).println("Hello Bob.");
    }

    @Test
    void printfArgAlone() {
        console.printf("{}", "Bob");

        Mockito.verify(out, Mockito.atLeastOnce()).println("Bob");
    }

    @Test
    void printfArgAtStart() {
        console.printf("Hello {}", "Bob");

        Mockito.verify(out, Mockito.atLeastOnce()).println("Hello Bob");
    }

    @Test
    void printfArgAtEnd() {
        console.printf("{}, hello.", "Bob");

        Mockito.verify(out, Mockito.atLeastOnce()).println("Bob, hello.");
    }

    @Test
    void printfMultiArg() {
        console.printf("Hello {}, {} and {}.", "Bob", "Jim", "Fred");

        Mockito.verify(out, Mockito.atLeastOnce()).println("Hello Bob, Jim and Fred.");
    }

    @Test
    void printfWithFullIndexing() {
        console.printf("Hello {1} and {0}.", "Bob", "Jim");

        Mockito.verify(out, Mockito.atLeastOnce()).println("Hello Jim and Bob.");
    }

    @Test
    void printfWithMixOfAutoAndIndex() {
        console.printf("Hello {} and {1}.", "Bob", "Jim");

        Mockito.verify(out, Mockito.atLeastOnce()).println("Hello Bob and Jim.");
    }

    @Test
    void printfWithReusedArg() {
        console.printf("Hello {}, {} and {0}.", "Bob", "Jim");

        Mockito.verify(out, Mockito.atLeastOnce()).println("Hello Bob, Jim and Bob.");
    }

    @Test
    void printfWithUnusedArg() {
        console.printf("Hello {}, {} and {0}.", "Bob", "Jim", "Random tag-along");

        Mockito.verify(out, Mockito.atLeastOnce()).println("Hello Bob, Jim and Bob.");
    }

    @Test
    void printfWithArgTypes() {
        console.printf("Hello {}, number {}.", "Bob", 1);

        Mockito.verify(out, Mockito.atLeastOnce()).println("Hello Bob, number 1.");
    }

    @Test
    void printfWithNewlines() {
        console.printf("Hello {}.\nHello {}.", "Bob", "Jim");

        Mockito.verify(out, Mockito.atLeastOnce()).println("Hello Bob.");
        Mockito.verify(out, Mockito.atLeastOnce()).println("Hello Jim.");
    }
}
