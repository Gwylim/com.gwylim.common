package com.gwylim.common.models;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;

public class ReferenceJsonTest {

    private static final PlayerRef REF = new PlayerRef("Player-TEST");

    public static class PlayerRef extends Reference {

        @JsonCreator
        public PlayerRef(final String ref) {
            super(ref);
        }

        @Override
        public String prefix() {
            return "Player";
        }
    }

    @Test
    void test3() throws IOException {
        final ObjectMapper objectMapper = new ObjectMapper();

        final String valueAsString = objectMapper.writeValueAsString(REF);

        Assertions.assertEquals("\"" + REF.getValue() + "\"", valueAsString);

        final PlayerRef playerRef = objectMapper.readValue(valueAsString, PlayerRef.class);

        Assertions.assertEquals(playerRef, REF);

    }
}
