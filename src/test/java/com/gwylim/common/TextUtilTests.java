package com.gwylim.common;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.Arrays;

class TextUtilTests {

    @ParameterizedTest(name = "\"{0}\" should be {1}")
    @CsvSource({ "Please enter a Integer, Please enter an Integer"})
    void testGrammar(final String input, final String expected) {
        Assertions.assertEquals(expected, TextUtil.grammar(input));
    }

    @Test
    void testRandomStringGenerator() {
        Assertions.assertEquals(0, TextUtil.secureRandomString(0).length());
        Assertions.assertEquals(100, TextUtil.secureRandomString(100).length());

        // Check that we are not just generating repeats:
        final String[] split = TextUtil.secureRandomString(100).split("");
        Assertions.assertFalse(Arrays.stream(split).allMatch(x -> x.equals(split[0])));

        System.out.println(TextUtil.secureRandomString(100));
    }
}
