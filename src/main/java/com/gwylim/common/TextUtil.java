package com.gwylim.common;

import org.apache.commons.lang3.StringUtils;

import java.security.SecureRandom;
import java.util.Objects;
import java.util.Random;
import java.util.stream.Collectors;

@SuppressWarnings("unused")
public final class TextUtil {
    private static final String RANDOM_STRING_CHARACTERS = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static final Random RANDOM = new SecureRandom();

    private TextUtil() {
        // Utility class
    }

    public static String grammar(final String in) {
        final String[] split = in.split("\\s");
        for (int i = 0; i < split.length; i++) {
            final int nextIndex = i + 1;
            if (nextIndex < split.length) {
                final String current = split[i];
                final String next = split[nextIndex];

                if ("a".equals(current)) {
                    if (next.matches("^[aeyiuoAEIOU].*")) {
                        split[i] = "an";
                    }
                }
            }
        }

        return String.join(" ", split);
    }

    public static String secureRandomString(final int length) {
        return RANDOM.ints(length, 0, RANDOM_STRING_CHARACTERS.length())
                           .mapToObj(RANDOM_STRING_CHARACTERS::charAt)
                           .map(Objects::toString)
                           .collect(Collectors.joining());
    }

    public static String truncate(final String source, final int length) {
        return truncate(source, length, "...");
    }

    public static String truncate(final String source, final int length, final String ellipsis) {
        if (source.length() > length) {
            return StringUtils.truncate(source, length - ellipsis.length()) + ellipsis;
        }
        else {
            return source;
        }
    }

    public static int countOccurrences(final String substring, final String string) {
        return StringUtils.countMatches(string, substring);
    }
}
