package com.gwylim.common;

import java.util.Comparator;
import java.util.concurrent.ThreadLocalRandom;

@SuppressWarnings("unused")
public final class StreamUtil {

    private StreamUtil() {
        // Utility class
    }

    public static <T> Comparator<T> randomly() {
        return (o1, o2) -> {
            if (o1.equals(o2)) {
                return 0;
            }
            return (ThreadLocalRandom.current().nextBoolean()) ? 1 : -1;
        };
    }
}
