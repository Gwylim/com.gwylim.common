package com.gwylim.common.exceptions;

import com.gwylim.common.models.Reference;

@SuppressWarnings("unused")
public class NotFoundException extends RuntimeException {
    private final Reference reference;

    public NotFoundException(final Reference reference) {
        super("Could not find resource for " + reference.getValue());
        this.reference = reference;
    }

    public Reference getReference() {
        return reference;
    }
}
