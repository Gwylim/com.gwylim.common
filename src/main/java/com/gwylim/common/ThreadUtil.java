package com.gwylim.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;

@SuppressWarnings("unused")
public final class ThreadUtil {
    private static final Logger LOG = LoggerFactory.getLogger(ThreadUtil.class);

    private static final AtomicInteger UNIQUIFY_COUNTER = new AtomicInteger();

    private ThreadUtil() {
        // Utility class
    }

    public static Thread daemonRun(final Runnable runnable) {
        final Thread thread = new Thread(runnable, "Daemon-" + UNIQUIFY_COUNTER.incrementAndGet());
        thread.setDaemon(true);
        thread.start();

        return thread;
    }

    /**
     * Wait "forever" for the condition to become true.
     * NB: These are best avoided, use push instead of polling.
     * @param condition The condition to wait for.
     */
    public static void waitFor(final Supplier<Boolean> condition) throws InterruptedException {
        waitFor(condition, Duration.ofDays(Integer.MAX_VALUE), () -> { });
    }

    /**
     * Wait for the condition to become true for the specified timeout.
     * NB: Timeout is the minimum time to wait, thread scheduling may result in waiting a bit longer for the condition.
     * NB: These are best avoided, use push instead of polling.
     * @param condition The condition to wait for.
     * @param timeout The time to wait on the condition.
     * @return true if the condition is met, false otherwise.
     */
    public static boolean waitFor(final Supplier<Boolean> condition, final Duration timeout) throws InterruptedException {
        return waitFor(condition, timeout, () -> { });
    }

    /**
     * Wait for the condition to become true for the specified timeout.
     * Run the supplied orElse in the event that the condition never becomes true.
     * NB: Timeout is the minimum time to wait, thread scheduling may result in waiting a bit longer for the condition.
     * NB: These are best avoided, use push instead of polling.
     * @param condition The condition to wait for.
     * @param timeout The time to wait on the condition.
     * @param orElse An action to perform in the case that the condition is never met.
     * @return true if the condition is met, false otherwise.
     */
    public static boolean waitFor(final Supplier<Boolean> condition, final Duration timeout, final Runnable orElse) throws InterruptedException {
        final long start = System.currentTimeMillis();
        final long limit = start + timeout.toMillis();

        while (!condition.get()) {
            if (System.currentTimeMillis() > limit) {
                orElse.run();
                return false;
            }
            ThreadUtil.delay(Duration.ofMillis(1));
        }

        return true;
    }

    @FunctionalInterface
    public interface DaemonController {
        void stop();
    }

    public static DaemonController daemonLoop(final Runnable action) {
        return daemonLoop(action, () -> { });
    }

    public static DaemonController daemonLoop(final Runnable action, final Duration loopDelay) {
        return daemonLoop(action, () -> { }, loopDelay);
    }

    public static DaemonController daemonLoop(final Runnable action, final Runnable cleanup) {
        return daemonLoop(action, cleanup, Duration.ZERO);
    }

    public static DaemonController daemonLoop(final Runnable action, final Runnable cleanup, final Duration loopDelay) {
        final AtomicBoolean stop = new AtomicBoolean(false);

        final Thread thread = daemonRun(() -> {
            while (!stop.get()) {
                action.run();
                safeDelay(loopDelay);
            }
            cleanup.run();
        });

        return () -> {
            stop.set(true);
            thread.interrupt();
            try {
                thread.join();
            }
            catch (final InterruptedException e) {
                LOG.warn("Interrupted while waiting for daemon to die.");
                Thread.currentThread().interrupt();
            }
        };
    }

    public static void run(final Runnable runnable) {
        new Thread(runnable, "Run-" + UNIQUIFY_COUNTER.incrementAndGet()).start();
    }


    public static void daemonRun(final Duration delay, final Runnable runnable) {
        run(delay, runnable, true);
    }


    public static void run(final Duration delay, final Runnable runnable) {
        run(delay, runnable, false);
    }

    public static void run(final Duration delay, final Runnable runnable, final boolean daemon) {
        final String name = (!delay.isZero() ? "Delayed-" : "")
                          + (daemon ? "Daemon-" : "")
                          + "Run-" + UNIQUIFY_COUNTER.incrementAndGet();

        final Thread thread = new Thread(() -> {
            safeDelay(delay);
            runnable.run();
        }, name);
        thread.setDaemon(daemon);
        thread.start();
    }

    public static void safeDelay(final Duration duration) {
        try {
            Thread.sleep(duration.toMillis());
        }
        catch (final InterruptedException e) {
            LOG.warn("Delayed run thread interrupted, skipping safeDelay.");
            Thread.currentThread().interrupt();
        }
    }

    public static void delay(final Duration duration) throws InterruptedException {
        Thread.sleep(duration.toMillis());
    }
}
