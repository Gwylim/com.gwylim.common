package com.gwylim.common.models;

import com.fasterxml.jackson.annotation.JsonValue;
import com.google.common.base.Objects;

import java.util.UUID;

@SuppressWarnings("unused")
public abstract class Reference {
    private final String identifier;

    protected Reference() {
        identifier = prefix() + "-" + generateUniqueString();
    }

    public Reference(final String ref) {
        if (ref == null) {
            throw new IllegalArgumentException(getClass().getSimpleName() + " cannot be created with null value.");
        }
        if (!ref.startsWith(prefix() + "-")) {
            throw new IllegalArgumentException(getClass().getSimpleName() + " must start with " + prefix() + "; " + ref + " did not.");
        }
        identifier = ref;
    }

    public abstract String prefix();

    @JsonValue
    public String getValue() {
        return identifier;
    }

    @Override
    public String toString() {
        return identifier;
    }


    protected String generateUniqueString() {
        return UUID.randomUUID().toString();
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final Reference reference = (Reference) o;
        return Objects.equal(identifier, reference.identifier);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(identifier);
    }
}
