package com.gwylim.common.models;

import com.fasterxml.jackson.annotation.JsonValue;
import com.google.common.base.Objects;

/**
 *  Increase type safety by using typed wrappers of classes like String.
 *  @param <T> The type that is wrapped.
 */ 
public abstract class Meaningful<T> {

    private final T identifier;

    public Meaningful(final T identifier) {
        if (identifier == null) {
            throw new IllegalArgumentException(getClass().getName() + " cannot be created with a null argument.");
        }
        this.identifier = identifier;
    }

    @JsonValue
    public final T getValue() {
        return identifier;
    }

    @Override
    public final String toString() {
        return identifier.toString();
    }


    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final Meaningful<?> reference = (Meaningful<?>) o;
        return Objects.equal(identifier, reference.identifier);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(identifier);
    }
}

