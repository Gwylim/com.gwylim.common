package com.gwylim.common;

import net.jodah.typetools.TypeResolver;
import org.springframework.core.annotation.AnnotationUtils;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@SuppressWarnings("unused")
public final class ReflectionUtil {

    private ReflectionUtil() {
        // Utility class
    }

    public static <T extends Annotation> Optional<T> getAnnotation(final Method m, final Class<T> annotation) {
        return Optional.ofNullable(AnnotationUtils.findAnnotation(m, annotation));
    }

    public static boolean hasAnnotation(final Method m, final Class<? extends Annotation> annotation) {
        return getAnnotation(m, annotation).isPresent();
    }


    public static <T extends Annotation> Optional<T> getAnnotation(final Object instance, final Class<T> annotation) {
        return getAnnotation(instance.getClass(), annotation);
    }

    public static <T extends Annotation> Optional<T> getAnnotation(final Class<?> clazz, final Class<T> annotation) {
        return Optional.ofNullable(AnnotationUtils.findAnnotation(clazz, annotation));
    }

    public static boolean hasAnnotation(final Class<?> clazz, final Class<? extends Annotation> annotation) {
        return getAnnotation(clazz, annotation).isPresent();
    }

    /**
     * Get the class that fills the generic argument for the class which is a super type of the given class.
     * @param onClass               The concrete class we are looking from.
     * @param extendedGenericClass  The generic class that is extended and has the generic parameter we are interested in.
     * @param <T>                   The generic class.
     * @return The Class object that the type parameter is bound to.
     */
    public static <T> Class<?> getGenericClass(final Class<? extends T> onClass, final Class<T> extendedGenericClass) {
        return TypeResolver.resolveRawArgument(extendedGenericClass, onClass);
    }

    /**
     * Get the class that fills the generic arguments for the class which is a super type of the given class.
     * @param <T>                   The generic class.
     * @param onClass               The concrete class we are looking from.
     * @param extendedGenericClass  The generic class that is extended and has the generic parameter we are interested in.
     * @return The Class objects that the type parameters are bound to.
     */
    public static <T> List<Class<?>> getGenericClasses(final Class<? extends T> onClass, final Class<T> extendedGenericClass) {
        return Arrays.asList(TypeResolver.resolveRawArguments(extendedGenericClass, onClass));
    }

    /**
     * Invoke the given method on the given object and return the return value as a Set.
     * If the returned value is not a collection it will be returned as a singleton set.
     * If it is null it will return the empty set.
     * If it is a collection it wil be converted to a set.
     */
    public static Set<Object> invokeToSet(final Object instance, final Method m) {
        try {
            final Object returnValue = m.invoke(instance);
            if (returnValue != null) {
                if (returnValue instanceof Optional) {
                    final Optional<?> o = (Optional<?>) returnValue;
                    return o.<Set<Object>>map(Collections::singleton)
                            .orElse(Collections.emptySet());
                }
                else if (returnValue instanceof Collection) {
                    return new LinkedHashSet<>((Collection<?>) returnValue);
                }
                else {
                    return Collections.singleton(returnValue);
                }
            }
            else {
                return Collections.emptySet();
            }
        }
        catch (final RuntimeException | IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException("Failed to invoke method: " + m.getDeclaringClass().getSimpleName() + "." + m.getName(), e);
        }
    }

    /**
     * Work out supertypes for a type.
     */
    public static LinkedHashSet<Class<?>> getSuperTypes(final Class<?> type) {
        final LinkedHashSet<Class<?>> classes = new LinkedHashSet<>();
        classes.add(type);
        if (type.getSuperclass() != Object.class) {
            classes.addAll(getSuperTypes(type.getSuperclass()));
        }
        classes.addAll(Arrays.asList(type.getInterfaces()));
        return classes;
    }


    public static Class<?> getClassByName(final String name) {
        try {
            return Class.forName(name);
        }
        catch (final ClassNotFoundException e) {
            throw new RuntimeException("Failed to get class by name: " + name, e);
        }
    }


    public static String getMethodIdentifier(final Method method) {
        return method.getDeclaringClass().getSimpleName() + "#" + method.getName();
    }
}
