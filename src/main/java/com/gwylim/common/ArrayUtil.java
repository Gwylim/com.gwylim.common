package com.gwylim.common;

import java.lang.reflect.Array;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.IntStream;
import java.util.stream.Stream;

@SuppressWarnings("unused")
public final class ArrayUtil {

    private ArrayUtil() {
        // Utility class
    }

    /**
     * Create a new 2d array initialised by the provided function.
     * @param xSize     The x size of the array.
     * @param ySize     The y size of the array.
     * @param source     The function source of the values.
     * @param finalClass The class of the array.
     * @param <V>        The type of the array.
     * @return An new array initialized by the source function.
     */
    @SuppressWarnings("unchecked")
    public static <V> V[][] initialise(final int xSize, final int ySize, final Class<V> finalClass, final BiFunction<Integer, Integer, V> source) {
        final V[][] out = (V[][]) Array.newInstance(finalClass, xSize, ySize);

        IntStream.range(0, out.length).forEach(x ->
                IntStream.range(0, out[x].length).forEach(y ->
                        out[x][y] = source.apply(x, y)
                )
        );

        return out;
    }


    /**
     * Create a new 2-dimensional array the same shape as the input of a the specified type using the specified converter.
     * @param source        The source 2 dimensional array.
     * @param converter     The converter to make the nre type from the old.
     * @param finalClass    The tye of the resultant array.
     * @param <U>           The type of the input array.
     * @param <V>           The type of the resulting array.
     * @return              A new array of the new type the same shape as the input array.
     */
    @SuppressWarnings("unchecked")
    public static <U, V> V[][] mapValues(final U[][] source, final Function<U, V> converter, final Class<V> finalClass) {
        final V[][] newState = (V[][]) Array.newInstance(finalClass, source.length, source[0].length);

        IntStream.range(0, source.length).forEach(x ->
            IntStream.range(0, source[x].length).forEach(y ->
                newState[x][y] = converter.apply(source[x][y])
            )
        );

        return newState;
    }

    /**
     * Pass over a pair of same shape 2 dimensional arrays and pass both to a consumer.
     * @param u         Array 1.
     * @param v         Array 2.
     * @param consumer  The consumer that will receive pairs of elements.
     * @param <U>       The type of array 1.
     * @param <V>       The type of array 2.
     */
    public static <U, V> void parallelConsume(final U[][] u, final V[][] v, final BiConsumer<U, V> consumer) {
        if (u.length == 0) {
            return;
        }

        IntStream.range(0, u.length).forEach(x ->
            IntStream.range(0, u[0].length).forEach(y ->
                consumer.accept(u[x][y], v[x][y])
            )
        );
    }

    public static <T> Stream<Cell<T>> stream(final T[][] a) {
        return IntStream.range(0, a.length).boxed().flatMap(x ->
               IntStream.range(0, a[0].length).mapToObj(y -> new Cell<>(x, y, a[x][y]))
        );
    }

    public static final class Cell<T> {
        private final int x;
        private final int y;
        private final T value;

        private Cell(final int x, final int y, final T value) {
            this.x = x;
            this.y = y;
            this.value = value;
        }

        public int getX() {
            return x;
        }
        public int getY() {
            return y;
        }

        public T getValue() {
            return value;
        }
    }
}
