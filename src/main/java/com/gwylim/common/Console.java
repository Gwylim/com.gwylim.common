package com.gwylim.common;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang3.StringUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Function;
import java.util.function.Predicate;

import static com.gwylim.common.ThreadUtil.daemonLoop;
import static java.nio.charset.StandardCharsets.UTF_8;

@SuppressWarnings("unused")
public class Console extends ConsoleOutput {
    private static final Map<Class<?>, Function<String, ?>> CONVERTERS = ImmutableMap.of(
            Integer.class, Integer::parseInt,
            Double.class, Double::parseDouble,
            String.class, (Function<String, String>) s -> {
                if (StringUtils.isBlank(s)) {
                    throw new IllegalArgumentException("Console was blank");
                }
                return s;
            }
    );

    private final InputStream in;
    private final BufferedReader buffer;

    private final ThreadUtil.DaemonController controller;
    private final Semaphore valueWait = new Semaphore(0);

    private Thread waitingThread;
    private String next;

    public Console() {
        this(System.in, System.out);
    }

    Console(final InputStream inStream,
            final PrintStream outStream) {

        super(outStream);

        this.in = inStream;

        buffer = new BufferedReader(new InputStreamReader(in, UTF_8));

        controller = daemonLoop(
            () -> {
                try {
                    if (in.available() > 0) {
                        next = buffer.readLine();
                        valueWait.release();
                    }
                }
                catch (final IOException e) {
                    throw new RuntimeException("Error reading from input.");
                }
            }
        );
    }

    public void terminate() {
        controller.stop();
    }


    /**
     * Wait for newline then just continue.
     */
    public void enterToContinue() throws SupersededException {
        enterToContinue("Press return to continue.");
    }

    /**
     * Wait for newline then just continue.
     */
    public void enterToContinue(final String prompt) throws SupersededException {
        read(s -> s, prompt, Collections.emptyList());
    }

    /**
     * Read non-blank String.
     */
    public String read(final String prompt) throws SupersededException {
        return read(String.class, prompt);
    }

    @SuppressWarnings("unchecked")
    public <T> T read(final Class<T> clazz, final String prompt) throws SupersededException {
        final Function<String, T> converter = (Function<String, T>) CONVERTERS.get(clazz);
        return read(converter, prompt, Collections.emptyList());
    }

    @SuppressWarnings("unchecked")
    public <T> T read(final Class<T> clazz, final String prompt, final Predicate<T> c1) throws SupersededException {
        final Function<String, T> converter = (Function<String, T>) CONVERTERS.get(clazz);
        final List<Predicate<T>> constraints = ImmutableList.of(c1);
        return read(converter, prompt, constraints);
    }

    @SuppressWarnings("unchecked")
    public <T> T read(final Class<T> clazz, final String prompt, final Predicate<T> c1, final Predicate<T> c2) throws SupersededException {
        final Function<String, T> converter = (Function<String, T>) CONVERTERS.get(clazz);
        final List<Predicate<T>> constraints = ImmutableList.of(c1, c2);
        return read(converter, prompt, constraints);
    }

    @SuppressWarnings("unchecked")
    public <T> T read(final Class<T> clazz, final String prompt, final Predicate<T>... constraints) throws SupersededException {
        final Function<String, T> converter = (Function<String, T>) CONVERTERS.get(clazz);
        return read(converter, prompt, Arrays.asList(constraints));
    }

    public <T> T read(final Function<String, T> converter, final String prompt, final Collection<Predicate<T>> constraints) throws SupersededException {
        while (true) {
            final String line = readLine(prompt);
            try {
                final T value = converter.apply(line);

                final boolean passConstraints = constraints.stream()
                        .allMatch(c -> c.test(value));

                if (passConstraints) {
                    return value;
                }
                else {
                    // TODO: Improve this messaging...
                    printf("Please enter a sensible value.");
                }
            }
            catch (final RuntimeException e) {
                final Class<?> converterClass = ReflectionUtil.getGenericClasses(converter.getClass(), Function.class).get(1);
                printf("Please enter a {}.", converterClass.getSimpleName());
            }
        }
    }

    /**
     * Read the next line and block until there is one.
     * @throws SupersededException if something else requets input.
     */
    private String readLine(final String prompt) throws SupersededException {
        printf(prompt);
        return readLine();
    }


    /**
     * Break out of the current read wait.
     */
    public synchronized void escape() {
        if (waitingThread != null) {
            waitingThread.interrupt();
            waitingThread = null;
        }
    }


    private final Lock checkLock = new ReentrantLock();
    private final Lock lock = new ReentrantLock();

    private String readLine() throws SupersededException {

        try {
            checkLock.lock();

            synchronized (this) {
                if (waitingThread != null) {
                    waitingThread.interrupt();
                }
                waitingThread = Thread.currentThread();
            }


            // Wait a "long" time for the lock to release as we should have interrupted the other thread...
            if (!lock.tryLock(1, TimeUnit.SECONDS)) {
                throw new RuntimeException("Failed to obtain lock.");
            }
        }
        catch (final InterruptedException e) {
            lock.unlock();
            throw new RuntimeException("Failed to obtain lock.");
        }
        finally {
            checkLock.unlock();
        }

        try {
            final String line = waitForNextLine();
            synchronized (this) {
                waitingThread = null;
            }
            return line;
        }
        catch (final InterruptedException e) {
            throw new SupersededException(e);
        }
        finally {
            // Always release the lock for the next thread.
            lock.unlock();
        }
    }


    private String waitForNextLine() throws InterruptedException {
        next = null; // Don't allow pre-entry.
        valueWait.drainPermits();
        valueWait.acquire();
        final String mine = next;
        next = null;
        return mine;
    }


    public static class SupersededException extends Exception {
        SupersededException(final InterruptedException e) {
            super(e);
        }
    }
}
