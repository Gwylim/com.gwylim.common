package com.gwylim.common;

import java.io.PrintStream;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicInteger;

import static com.gwylim.common.TextUtil.grammar;

@SuppressWarnings("unused")
public class ConsoleOutput {

    private final PrintStream out;

    public ConsoleOutput() {
        this(System.out);
    }

    ConsoleOutput(final PrintStream outStream) {
        this.out = outStream;
    }

    public synchronized void printf(final String format, final Object... args) {
        final String indexed = index(format);

        final String[] lines = indexed.split("\\n");

        Arrays.stream(lines).forEach(l -> {
            final String message = MessageFormat.format(l, args);
            this.out.println(grammar(message));
        });
    }

    private String index(final String format) {
        final String[] split = (" " + format + " ").split("\\{}");

        final AtomicInteger count = new AtomicInteger(0);
        final String indexed = Arrays.stream(split)
                                     .reduce((a, b) -> a + "{" + count.getAndIncrement() + "}" + b)
                                     .orElse(format);

        return indexed.substring(1, indexed.length() - 1);
    }

    public synchronized void block(final Runnable runnable) {
        runnable.run();
    }
}
