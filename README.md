# README #

### What is this repository for? ###

A set of standard utility functions for use in other projects.

### How do I get set up? ###

Add as a git submodule to other projects:

    git submodule add git@bitbucket.org:Gwylim/com.gwylim.common.git java-lib/com.gwylim.common